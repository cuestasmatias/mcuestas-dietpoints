# Introducción #

DietPoints es una aplicación web que permite que usuarios registrados carguen las comidas y bebidas que consumieron durante el día. Con los datos cargados logra valorizar cada comida en "diet points", que se van sumando a lo largo del día. Mientras los diet points diarios estén por debajo del límite configurado, el usuario está siguiendo su plan de alimentación.
La aplicación está desarrollada como una solución de Microsoft Visual Studio.


### Instrucciones ###

1. Obtener el código de la solución desde git clone https://bitbucket.org/xsidesolutions/ex024-diet-points
2. Verificar connection strings de los archivos app.config (proyecto de tests) y web.config (proyecto web), modificar si se desea utilizar otra instancia de DB que no sea .\sqlexpress u otro método de autenticación.
3. Crear las bases de datos indicadas en los connection strings. Tablas y datos iniciales se generarán automáticamente cuando se ejecute la aplicación (tests y web).
