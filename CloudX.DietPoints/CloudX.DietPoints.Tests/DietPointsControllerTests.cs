﻿using System.Net;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CloudX.DietPoints.Tests.Support;
using CloudX.DietPoints.Web.Models;
using CloudX.DietPoints.Domain.Model;
using System;
using CloudX.DietPoints.Web.Controllers;
using System.Web.Http.Results;
using CloudX.DietPoints.Domain;
using System.Linq;
using System.Collections.Generic;

namespace CloudX.DietPoints.Tests
{
    [TestClass]
    public class DietPointsControllerTests : BaseApiControllerTests<DietPointsController>
    {
        [TestInitialize]
        public override void TestInitialize()
        {
            base.TestInitialize();
        }

        [TestCleanup]
        public override void TestCleanup()
        {
            base.TestCleanup();
        }

        [TestMethod]
        public void DietPointsValidation()
        {
            var user = Context.Users.Single(x => x.Id == LoggedUserId);
            var foodTypeSteak = Context.FoodTypes.Single(x => x.Id == SteakId);

            var asado10 = 
                new Entry.Builder("Asado", foodTypeSteak, 100, DateTime.Now, user)
                .SetFatPercentage(10)    
                .Build();

            Assert.AreEqual((int)(foodTypeSteak.CaloriesMultiplier * asado10.Calories), asado10.DietPoints);

            var asado30 = new Entry.Builder("Asado", foodTypeSteak, 100, DateTime.Now, user)
                .SetFatPercentage(30)
                .Build();

            Assert.AreEqual((int)(foodTypeSteak.CaloriesMultiplier * asado30.Calories * 1.1D), asado30.DietPoints);
                        

            var asado70 = new Entry.Builder("Asado", foodTypeSteak, 100, DateTime.Now, user)
                .SetFatPercentage(70)
                .Build();

            Assert.AreEqual((int)(foodTypeSteak.CaloriesMultiplier * asado70.Calories * 1.2D), asado70.DietPoints);

            var foodTypeDairy = Context.FoodTypes.Single(x => x.Id == DairyId);

            var cheseLowInFat = new Entry.Builder("Chese low in fat", foodTypeDairy, 50, DateTime.Now, user)
                .SetLowFat(true)
                .Build();

            Assert.AreEqual((int)(foodTypeDairy.CaloriesMultiplier * cheseLowInFat.Calories * 0.8D), cheseLowInFat.DietPoints);

            var chese = new Entry.Builder("Chese", foodTypeDairy, 50, DateTime.Now, user)
                .SetLowFat(false)
                .Build();

            Assert.AreEqual((int)(foodTypeDairy.CaloriesMultiplier * cheseLowInFat.Calories), chese.DietPoints);

            var foodTypeSalad = Context.FoodTypes.Single(x => x.Id == SaladId);
            
            var salad3000 = new Entry.Builder("Salad", foodTypeSalad, 3000, DateTime.Now, user)            
            .Build();

            Assert.AreEqual(947, salad3000.DietPoints);


            var salad400= new Entry.Builder("Salad", foodTypeSalad, 400, DateTime.Now, user).Build();

            Assert.AreEqual((int)(salad400.Calories * foodTypeSalad.CaloriesMultiplier), salad400.DietPoints);

        }

        [TestMethod]
        public void ConfigureDietPointsAndGetExpectedDailyDietPoints()
        {
            var model = new ConfigureDietPointsViewModel
            {
                ExpectedDailyDietPoints = 1200
            };

            var put = Controller.ConfigureDietPoints(model) as StatusCodeResult;

            Assert.IsNotNull(put);
            Assert.AreEqual(HttpStatusCode.NoContent, put.StatusCode);

            var get = Controller.Get() as OkNegotiatedContentResult<DietPointsViewModel>;

            Assert.IsNotNull(get);
            Assert.AreEqual(1200, get.Content.ExpectedDailyDietPoints);

            model.ExpectedDailyDietPoints = 1000;

            put = Controller.ConfigureDietPoints(model) as StatusCodeResult;

            Assert.IsNotNull(put);
            Assert.AreEqual(HttpStatusCode.NoContent, put.StatusCode);

            get = Controller.Get() as OkNegotiatedContentResult<DietPointsViewModel>;

            Assert.IsNotNull(get);
            Assert.AreEqual(1000, get.Content.ExpectedDailyDietPoints);
        }

        private void SaveEntry(string meal, int calories, DateTime date, int foodTypeId)
        {
            var user = Context.Users.Single(x => x.Id == LoggedUserId);
            var foodType = Context.FoodTypes.Single(x => x.Id == foodTypeId);

            var entry = new Entry.Builder(meal, foodType, calories, date, user).Build();

            Context.Entries.Add(entry);
            Context.SaveChanges();
        }

        [TestMethod]
        public void GetLastWeekResume()
        {            
            SaveEntry("Tomato Salad", 300, DateTime.Now.AddDays(-1), SaladId);
            SaveEntry("Tomato Salad", 300, DateTime.Now.AddDays(-2), SaladId);
            SaveEntry("Tomato Salad", 300, DateTime.Now.AddDays(-3), SaladId);
            SaveEntry("Tomato Salad", 300, DateTime.Now.AddDays(-4), SaladId);
            SaveEntry("Asado", 1500, DateTime.Now.AddDays(-5), SteakId);
            SaveEntry("Tiramisu", 700, DateTime.Now.AddDays(-5), DessertId);

            var resume = Controller.GetLastWeekResume() as OkNegotiatedContentResult<List<DietPointsResumeItem>>;

            Assert.IsNotNull(resume);

        }

        [TestMethod]
        public void TodaysDietPoints()
        {
            SaveEntry("Asado", 1500, DateTime.Now, SteakId);
            SaveEntry("Tiramisu", 700, DateTime.Now, DessertId);            
            SaveEntry("Tomato Salad", 300, DateTime.Now.AddDays(-1), SaladId);

            var get = Controller.Get() as OkNegotiatedContentResult<DietPointsViewModel>;

            Assert.IsNotNull(get);

            Assert.AreEqual(3750, get.Content.TodaysDietPoints);
        }
    }
}