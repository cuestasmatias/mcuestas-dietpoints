﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Web.Http;
using Microsoft.AspNet.Identity;
using CloudX.DietPoints.Web.Models;
using CloudX.DietPoints.Web.Support;
using CloudX.DietPoints.Domain;

namespace CloudX.DietPoints.Web.Controllers
{
    [Authorize]
    public class DietPointsController : BaseApiController
    {
        [Route("api/me/dietpoints/GetLastWeekResume")]
        [HttpGet]
        public IHttpActionResult GetLastWeekResume()
        {
            var userId = User.Identity.GetUserId();
            var dateTo = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day - 1, 0, 0, 0);
            var dateFrom = dateTo.AddDays(-6);


            var entries = Context.Entries
                          .GroupBy(x =>
                            DbFunctions.CreateDateTime(x.Date.Year, x.Date.Month, x.Date.Day, 0, 0, 0).Value
                          )
                          .Where(w => w.Key <= dateTo && w.Key >= dateFrom)
                          .Select(s => new DietPointsResumeItem
                          {
                              Date = s.Key,
                              DietPointsPerDay = s.Sum(su => su.DietPoints)
                          })
                          .OrderByDescending(o => o.Date)
                          .ToList();

            return Ok(entries);
        }

        [Route("api/me/dietpoints")]
        [HttpGet]
        public IHttpActionResult Get()
        {
            var userId = User.Identity.GetUserId();

            var user = Context.Users.Where(x => x.Id == userId).SingleOrDefault();

            var model = new DietPointsViewModel
            {
                ExpectedDailyDietPoints = user.ExpectedDailyDietPoints,
                TodaysDietPoints = Context.Entries.Where(x => x.User.Id == userId && DbFunctions.TruncateTime(x.Date) == DbFunctions.TruncateTime(DateTime.Today))
                                                  .Select(x => x.DietPoints)
                                                  .DefaultIfEmpty(0)
                                                  .Sum()
            };

            return Ok(model);
        }

        [Route("api/me/dietpoints/configuration")]
        [HttpPut]
        public IHttpActionResult ConfigureDietPoints(ConfigureDietPointsViewModel model)
        {
            var userId = User.Identity.GetUserId();

            var profile = Context.Users.SingleOrDefault(x => x.Id == userId);

            if (profile == null)
                return NotFound();

            profile.ExpectedDailyDietPoints = model.ExpectedDailyDietPoints;

            Context.SaveChanges();

            return NoContent();
        }
    }
}