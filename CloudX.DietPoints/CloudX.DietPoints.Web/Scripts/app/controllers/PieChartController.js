﻿app.controller("pieChartController", function ($scope, $uibModalInstance, percentages) {

    $scope.chartData = [];
    $scope.chartDataGraphic = [];

    $scope.total = 0;


    $scope.save = function() {
        $uibModalInstance.close($scope.model.dietPoints);
    };

    $scope.colors = ['red', 'blue', 'orange', 'green', '#E87E04', '#BDC3C7', '#66CC99']
    
    $scope.getTotal = function (percentages) {
        var total = 0;
        for (var i = 0; i < percentages.length; i++) {
            total += percentages[i].dietPoints;
        }
        return total;
    }

    $scope.getRandomColor = function () {
        var letters = '0123456789ABCDEF';
        var color = '#';
        for (var i = 0; i < 6; i++) {
            color += letters[Math.floor(Math.random() * 16)];
        }
        return color;
    }

    $scope.buildData = function (percentages) {
        $scope.chartData = [];
        $scope.total = $scope.getTotal(percentages);
        var currentGrades = 0;
        for (var i = 0; i < percentages.length; i++) {

            var size = 360 * percentages[i].dietPoints / $scope.total;
            var percentage = 100 * percentages[i].dietPoints / $scope.total;
            var color = $scope.colors[i] ? $scope.colors[i] : $scope.getRandomColor();

            var curGradesEnd = Math.round(currentGrades + size);
            var chartItem = {
                name: percentages[i].foodTypeName,
                gradesStart: currentGrades > 180 ? currentGrades - 360 : currentGrades,
                gradesSize: Math.round(size),
                color: color,
                percentage: percentage
            }

            $scope.addToGraphic(chartItem);
            $scope.chartData.push(chartItem);
            currentGrades = curGradesEnd
        }

        console.log($scope.chartData);
    }

    $scope.addToGraphic = function(chartItem) {
        if (chartItem.gradesSize > 180) {
            var secondItem = Object.assign({}, chartItem);
            secondItem.gradesSize = secondItem.gradesSize - 180;
            $scope.chartDataGraphic.push(secondItem);

            chartItem.gradesStart += secondItem.gradesSize;
            chartItem.gradesSize = 180;
        }

        $scope.chartDataGraphic.push(chartItem);
    }


    $scope.close = function() {
        $uibModalInstance.dismiss("cancel");
    };

    $scope.buildData(percentages);

});