﻿app.controller("entryFormController", function ($scope, $uibModalInstance, entry, entriesService, foodTypesService) {

    $scope.entry = Object.assign({}, { id: entry.id, meal: entry.meal, date: new Date() });

    if ($scope.entry.id !== 0) {
        entriesService.getById($scope.entry.id)
            .success(function (data) {
                $scope.entry = data
            }).error(function (response) {
                $scope.errors = parseErrors(response)
            })
    }

    foodTypesService.getAll()
        .success(function (foodTypes) {
            $scope.foodTypes = foodTypes;
        })
        .error(function (response) {
            $scope.errors = parseErrors(response);
        });

    $scope.foodTypes =
        $scope.errors = [];

    $scope.selectedMeal = {};

    $scope.save = function () {
        entriesService.save($scope.entry)
            .success(function () {
                $uibModalInstance.close();
            })
            .error(function (response) {
                $scope.errors = parseErrors(response);
            });
    };

    $scope.$watch('selectedMeal', function (selectedItem) {
        if (selectedItem) {
            if (selectedItem.type === "writing") {
                $scope.entry.meal = selectedItem.title
            }
            else {
                if (selectedItem.originalObject) {

                    var foodtype = $scope.foodTypes.filter((ft) => ft.id === selectedItem.originalObject.foodTypeId);

                    if (foodtype[0])
                        $scope.entry.foodType = foodtype[0];

                    $scope.entry.meal = selectedItem.originalObject.meal;
                    $scope.entry.calories = selectedItem.originalObject.calories;

                }
            }
        }
    }, true);

    $scope.close = function () {
        $uibModalInstance.dismiss("cancel");
    };
});