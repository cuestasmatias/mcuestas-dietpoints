﻿app.controller("dietPointsController", function ($scope, $uibModal, $filter, confirmDialog, dietPointsService, entriesService) {
    clearFilterAndSearch();
    refreshDietPoints();

    $scope.search = search;
    $scope.pageSize = 10;

    $scope.pageChanged = function () {
        getEntries();
    };

    $scope.deleteEntry = function (entry) {
        var options = {
            message: "Are you sure?",
            title: "Delete Entry",
            confirm: function () {
                entriesService.delete(entry.id).success(function () {
                    getEntries();
                    refreshDietPoints();
                });
            }
        };
        confirmDialog(options);
    };

    $scope.clearFilterAndSearch = clearFilterAndSearch;

    $scope.filterWhen = function () {
        var modalInstance = $uibModal.open({
            templateUrl: "/scripts/app/views/entriesFilter.html",
            controller: "entriesFilterController",
            resolve: {
                filter: function () { return $scope.filter }
            },
            windowClass: "filter-when"
        });

        modalInstance.result.then(function (filter) {
            $scope.filter = filter;
            getEntries();
        });
    }

    $scope.addNewEntry = function () {
        openEntryForm({ id: 0, meal: '' });
    };

    $scope.editEntry = function (entry) {
        openEntryForm(entry);
    };

    $scope.configureExpectedDietPointsPerDay = function () {
        var modalInstance = $uibModal.open({
            templateUrl: "/scripts/app/views/configureDietPointsPerDay.html",
            controller: "configureDietPointsPerDayController",
            resolve: {
                dietPoints: function () { return $scope.dietPoints.expectedDailyDietPoints }
            },
            windowClass: "expected-daily-dietPoints-configuration"
        });

        modalInstance.result.then(function (dietPoints) {
            dietPointsService.setDietPointsConfiguration({
                expectedDailyDietPoints: dietPoints
            }).success(refreshDietPoints);
        });
    };

    $scope.pieChart = function () {
        entriesService.getPieChart({
            page: $scope.currentPage,
            dateFrom: $scope.filter.dateFrom,
            dateTo: $scope.filter.dateTo,
            timeFrom: $scope.filter.timeFrom,
            timeTo: $scope.filter.timeTo
        }).success(function (response) {
            if (response.length > 0)
                var modalInstance = $uibModal.open({
                    templateUrl: "/scripts/app/views/PieChart.html",
                    controller: "pieChartController",
                    resolve: {
                        percentages: function () { return response }
                    },
                    windowClass: "pie-chart"
                });
        });
    }

    function getEntries() {
        entriesService.getEntries({
            page: $scope.currentPage,
            dateFrom: $scope.filter.dateFrom,
            dateTo: $scope.filter.dateTo,
            timeFrom: $scope.filter.timeFrom,
            timeTo: $scope.filter.timeTo
        }).success(function (response) {
            $scope.pageSize = response.pageSize;
            $scope.totalRecordsCount = response.totalRecords;
            $scope.entries = response.items;
        });
    };

    function search() {
        $scope.currentPage = 1;
        getEntries();
    };

    function openEntryForm(entry) {
        var modalInstance = $uibModal.open({
            templateUrl: "/scripts/app/views/entryForm.html",
            controller: "entryFormController",
            resolve: {
                entry: function () { return entry }
            }
        });

        modalInstance.result.then(function () {
            getEntries();
            refreshDietPoints();
        });
    }

    function refreshDietPoints() {
        dietPointsService.getDietPointsInfo()
            .success(function (response) {
                $scope.dietPoints = response;
                dietPointsService.getLastWeekResume()
                    .success(function (response) {
                        generateWeekResume(response);
                    })
            }); 
    }

    function generateWeekResume(dietpointsWeekResumeOriginal) {
        $scope.dietpointsPerformance = [];
        
        var totalDietpointsWeek = 0;
        var totalPerformance = 0;
        var bellowExpected = $scope.dietPoints.expectedDailyDietPoints * 0.4;
        var aboveExpected = $scope.dietPoints.expectedDailyDietPoints * 1.5;

        var bellowRange = bellowExpected - $scope.dietPoints.expectedDailyDietPoints;
        var aboveRange = aboveExpected - $scope.dietPoints.expectedDailyDietPoints;
                
        for (var i = 0; i < dietpointsWeekResumeOriginal.length; i++) {
            var performance = 0;
            totalDietpointsWeek += dietpointsWeekResumeOriginal[i].dietPointsPerDay;
            if (dietpointsWeekResumeOriginal[i].dietPointsPerDay < bellowExpected
                || dietpointsWeekResumeOriginal[i].dietPointsPerDay > aboveExpected)
                performance = 0;
            else
            {
                var range = dietpointsWeekResumeOriginal[i].dietPointsPerDay - $scope.dietPoints.expectedDailyDietPoints;
                if (range <= 0) {
                    performance = Math.round(100 - (range * 100 / bellowRange));
                }
                else
                {
                    performance = Math.round(100 - (range * 100 / aboveRange));
                }
            }

            $scope.dietpointsPerformance.push({
                performance: performance,
                day: getDayDescription(new Date(dietpointsWeekResumeOriginal[i].date).getDay()),
                faceImg: getFaceImg(performance),
                dietPoints: dietpointsWeekResumeOriginal[i].dietPointsPerDay
            });

            totalPerformance += performance;
        }

        $scope.averagePerformance = Math.round(totalPerformance / dietpointsWeekResumeOriginal.length);
        $scope.averageFace = getFaceImg($scope.averagePerformance);
        $scope.isAbove = (totalDietpointsWeek / dietpointsWeekResumeOriginal.length) > $scope.dietPoints.expectedDailyDietPoints;
        $scope.dietPointsMessage = getMessage($scope.averagePerformance, $scope.isAbove);

    }

    function getMessage(percentage, isAbove) {
        if (percentage >= 85) {
            return 'You are the best'
        }
        if (percentage < 85 && percentage >= 75) {
            return 'You are doing well, but they could do better'
        }

        if (percentage < 75 && percentage >= 50) {
            return 'Try Again'
        }

        if (percentage < 50 && percentage >= 25) {
            if (isAbove) {
                return 'So Bad, you should eat less'
            }
            else
            {
                return 'So Bad, you should eat more'
            }
        }

        if (percentage < 25) {
            if (isAbove){
                return 'Obesity is bad for your health, you should eat less'
            } else {
                return 'Malnutrition is bad for your health, you should eat more'
            }
        }

    }

    function getFaceImg(percentage) {
        if (percentage >= 85) {
            return 'face1.png'
        }
        if (percentage < 85 && percentage >= 75) {
            return 'face2.png'
        }

        if (percentage < 75 && percentage >= 50) {
            return 'face3.png'
        }

        if (percentage < 50 && percentage >= 25) {
            return 'face4.png'
        }

        if (percentage < 25) {
            return 'face5.png'
        }
    }

    function getDayDescription(day) {
        switch (day) {
            case 0:
                return "Sunday";
                break;
            case 1:
                return "Monday";
                break;
            case 2:
                return "Tuesday";
                break;
            case 3:
                return "Wednesday";
                break;
            case 4:
                return "Thursday";
                break;
            case 5:
                return "Friday";
                break;
            case 6:
                return "Saturday";
        }
    }

    function clearFilterAndSearch() {
        $scope.filter = {
            dateFrom: null,
            dateTo: null,
            timeFrom: null,
            timeTo: null
        };
        search();
    }
});