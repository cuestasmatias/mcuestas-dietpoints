﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CloudX.DietPoints.Web.Models
{
    public class EntriesAutocompleteViewModel
    {
        public string Meal { get; set; }
        public int Calories { get; set; }
        public int FoodTypeId {get;set;}
        public string FoodTypeName { get; set; }

    }
}