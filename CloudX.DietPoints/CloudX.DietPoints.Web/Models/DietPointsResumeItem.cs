﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CloudX.DietPoints.Web.Models
{
    public class DietPointsResumeItem
    {
        [JsonProperty("date")]
        public DateTime Date { get; set; }

        [JsonProperty("dietPointsPerDay")]
        public int DietPointsPerDay { get; set; } 
    }
}