﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CloudX.DietPoints.Web.Models
{
    public class EntriesPieChartViewModel
    {
        public string FoodTypeName { get; set; }
        public double DietPoints { get; set; } 
    }
}