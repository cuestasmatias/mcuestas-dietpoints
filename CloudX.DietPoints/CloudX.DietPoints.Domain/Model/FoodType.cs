﻿namespace CloudX.DietPoints.Domain.Model
{
    public class FoodType
    {
        protected const int OojedConstant = 947;
        protected const double LowFatMultiplier = 0.8D;
        protected const double Meat25to50Multiplier = 1.1D;
        protected const double MeatMoreThan50Multiplier = 1.2D;

        public virtual int Id { get; set; }
        public virtual string Name { get; set; }
        public virtual double CaloriesMultiplier { get; set; }
        public virtual FoodGroup FoodGroup { get; set; }
        public virtual byte[] Picture { get; set; }

        public virtual int CalculateDietPoints(Entry entry)
        {
            double aditionalMultiplier = 1D;

            if (entry.FoodType.FoodGroup == FoodGroup.Vegetables)
            {
                if (entry.Calories > 1000)
                {
                    return OojedConstant;
                }
            }

            if (entry.FoodType.FoodGroup == FoodGroup.Dairy)
            {
                if (entry.LowFat)
                {
                    aditionalMultiplier = LowFatMultiplier;
                }
            }

            if (entry.FoodType.FoodGroup == FoodGroup.Meat)
            {                
                if(entry.FatPercentage >= 25 && entry.FatPercentage <= 50)
                {
                    aditionalMultiplier = Meat25to50Multiplier;
                }

                if(entry.FatPercentage > 50)
                {
                    aditionalMultiplier = MeatMoreThan50Multiplier;
                }
            }

            return (int)(entry.Calories * CaloriesMultiplier * aditionalMultiplier);
        }

    }
}
