﻿using System;

namespace CloudX.DietPoints.Domain.Model
{
    public class Entry
    {
        public virtual int Id { get; protected set; }
        public virtual string Meal { get; protected set; }
        public virtual FoodType FoodType { get; protected set; }
        public virtual int Calories { get; protected set; }
        public virtual int DietPoints { get; protected set; }
        public virtual int FatPercentage { get; protected set; }
        public virtual bool LowFat { get; protected set; }
        public virtual DateTime Date { get; protected set; }
        public virtual ApplicationUser User { get; protected set; }

        public class Builder
        {
            private Entry _entry = null;

            public Builder(Entry entry, string meal, FoodType foodtype, int calories, DateTime date, ApplicationUser user)
            {
                _entry = entry;
                _entry.Meal = meal;
                _entry.FoodType = foodtype;
                _entry.Calories = calories;
                _entry.Date = date;
                _entry.User = user;
            }

            public Builder(string meal, FoodType foodtype, int calories, DateTime date, ApplicationUser user)
            {
                _entry = new Entry
                {                    
                    Meal = meal,
                    FoodType = foodtype,
                    Calories = calories,
                    Date = date,
                    User = user
                };
            }

            public Builder SetId(int id)
            {
                _entry.Id = id;

                return this;
            }

            public Builder SetLowFat(bool lowFat)
            {
                if (_entry.FoodType.FoodGroup == FoodGroup.Dairy)
                    _entry.LowFat = lowFat;

                return this;
            }

            public Builder SetFatPercentage(int fatPercentage)
            {
                if(_entry.FoodType.FoodGroup == FoodGroup.Meat)
                    _entry.FatPercentage = fatPercentage;

                return this;
            }

            public Entry Build()
            {
                CalculateDietPoints();
                return _entry;
            }

            private void CalculateDietPoints()
            {
                _entry.DietPoints = _entry.FoodType.CalculateDietPoints(_entry);
            }

        }
    }
}